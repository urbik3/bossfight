"use strict"

window.app = window.app || {}
window.app.game = window.app.game || {}

window.app = (function(app, $, createjs) {

    app.stage = false
    app.renderer = false
    app.ticker = {}

    app.fps = 60
    app.mspf = 1000/app.fps

    app.changeFPS = fps => {
        app.fps = fps
        app.mspf = 1000/app.fps
    }

    let canvas
    app.canvas_width
    app.canvas_height

    app.initCallbacks = []
    app.addInitCallback = function(fn) {
        app.initCallbacks.push(fn)
    }

    /*
     * INIT & UTILS
     * */

    app.init = () => {
        // // INIT STAGE
        // app.stage = new createjs.Container()
        // app.stage.interactive = true
        //
        // app.renderer = PIXI.autoDetectRenderer(window.innerWidth, window.innerHeight)
        // app.renderer.autoResize = true
        // app.renderer.backgroundColor = 0xffffff
        // document.body.appendChild(app.renderer.view)
        // app.renderer.render(app.stage)
        //
        // document.addEventListener('contextmenu', e=>e.preventDefault(), false)
        //
        // // INIT RESIZE
        // let $window = $(window)
        // $window.resize(app.resize)
        // app.resize({}, true)
        //
        // // INIT APP
        // app.initCallbacks.forEach(fn=>fn(app))
        // app.play()

        // IniT STAGE
        canvas = document.createElement("canvas")
        document.body.appendChild(canvas)
        app.stage = new createjs.Stage(canvas)
        // app.stage.setClearColor("#000")

        // INIT RESIZE
        let $window = $(window)
        $window.resize(app.resize)
        app.resize({}, true)

        // INIT APP
        document.addEventListener('contextmenu', e=>e.preventDefault(), false)
        app.initCallbacks.forEach(fn=>fn(app))
        app.play()

        // var g = new createjs.Graphics();
        // g.setStrokeStyle(3);
        // g.beginStroke("#000000");
        // // g.beginFill("rgba(0,0,0,0)");
        // g.drawCircle(55, 55, 50);
        // g.endFill();
        // app.stage.addChild(g);
        // app.stage.update();
        //
        // var circle = new createjs.Shape();
        // circle.graphics.setStrokeStyle(3).beginStroke("red").drawCircle(0, 0, 50);
        // circle.x = 100;
        // circle.y = 100;
        // app.stage.addChild(circle);
        // app.stage.update();
    }

    // app.stop = () => {
    //     app.status = "stopped"
    // }

    // app.play = () => {
    //     app.ticker.then = Date.now()
    //     app.ticker.startTime = app.ticker.then
    //     app.status = "playing"
    //     app.draw()
    // }

    app.stop = () => {
        createjs.Ticker.reset()
    }

    app.play = () => {
        createjs.Ticker.init()
        createjs.Ticker.framerate = app.fps
        createjs.Ticker.addEventListener("tick", app.update)
    }

    // app.draw = () => {
    //     if(app.status !== "playing")
    //         return
    //
    //     requestAnimationFrame(app.draw)
    //     let now = Date.now(),
    //         deltaTime = now - app.ticker.then
    //
    //     if (deltaTime > app.mspf) {
    //         app.ticker.then = now - (deltaTime % app.mspf)
    //         app.update(deltaTime)
    //         app.renderer.render(app.stage)
    //     }
    // }

    app.update = (tick) => {
        // console.log("update", deltaTime)
        // app.module.update(
        //     deltaTime,
        //     app.module.config,
        //     Object.assign({}, app.module.state)
        // )
        app.game.update(tick.delta)
        app.stage.update()
    }

    app.resize = (e, firstResize = false) => {
        app.canvas_width = window.innerWidth
        app.canvas_height = window.innerHeight
        canvas.style.height = app.canvas_height+"px"
        canvas.style.width = app.canvas_width+"px"
        app.stage.canvas.height = app.canvas_height
        app.stage.canvas.width = app.canvas_width
        if(firstResize !== true)
            app.game.resize()
    }

    // app.basicAnimation = () => {
    //     let circle = new createjs.Shape()
    //     circle.graphics.beginFill("Crimson").drawCircle(0, 0, 50)
    //     circle.x = 100
    //     circle.y = 100
    //     app.stage.addChild(circle)
    //     // app.points["circle"] = circle
    //     // createjs.Tween.get(circle, {loop: true})
    //     //     .to({x: 400}, 1000, createjs.Ease.getPowInOut(4))
    //     //     .to({alpha: 0, y: 75}, 500, createjs.Ease.getPowInOut(2))
    //     //     .to({alpha: 0, y: 125}, 100)
    //     //     .to({alpha: 1, y: 100}, 500, createjs.Ease.getPowInOut(2))
    //     //     .to({x: 100}, 800, createjs.Ease.getPowInOut(2))
    // }

    return app
})(window.app, jQuery, createjs)