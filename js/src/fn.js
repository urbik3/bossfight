
const ID = () => '_' + Math.random().toString(36).substr(2, 9)

const randomInt = (min, max) => {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min + 1)) + min
}

const round = (num, floating = 2) =>
    Math.round(num * 10**floating) / 10**floating

const
    toRad = degree => degree*Math.PI/180,
    toDegree = rad => rad*180/Math.PI

const rad = rad =>
    rad < 0 ? Math.PI*2 + rad :
        rad > Math.PI*2 ? rad - Math.PI*2 :
            rad

const radBetween = (radian, radBottom, radTop) => {
    radian = rad(radian)

    // MORE THAN 2PI
    if(radTop > Math.PI*2)
        radTop = rad(radTop)
    if(radBottom > Math.PI*2)
        radBottom = rad(radBottom)

    // LESS THAN 0
    if (radBottom < 0 || radTop < 0) {
        if (radBottom < 0 && radTop < 0) {
            radBottom = rad(radBottom)
            radTop = rad(radTop)
        }
        else if (radBottom < 0 && radTop >= 0)
            radTop = rad(radTop)
    }

    if(radBottom > radTop)
        radBottom -= Math.PI*2

    if(radian > Math.PI*2+radBottom)
        radian -= Math.PI*2

    let isBetween = radian <= radTop && radian >= radBottom

    return isBetween
}

const logTime = (fn, msTreshold = 0, disable = false) => {
    let logging = !disable,
        startTime = new Date().getMilliseconds(),
        endTime,
        time,
        return_val

    return_val = fn()

    endTime = new Date().getMilliseconds()
    time = endTime - startTime
    if(logging && time >= msTreshold)
        console.log(time)

    return return_val
}

const generateArr = (start, end, space = 1) => {
    let arr = []
    for(let x = start; x <= end; x += space)
        arr.push(x)
    return arr
}

const eq = (a) => a
const blank = () => {}