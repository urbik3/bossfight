"use strict"

window.app = window.app || {}
window.app.boss = window.app.boss || {}
window.app.boss.behaviour = window.app.boss.behaviour || {}
window.app.boss.behaviour.beast = window.app.boss.behaviour.beast || {}

/*
 * BOSS BEHAVIOUR
 * */
window.app.boss.behaviour.beast = ((beast, behaviour, boss, app, createjs, TweenMax, EasingFunctions) => {

    beast.update = (instance, player, dt) => {
        // return [b.hitRound({hitCount: 7})
        // return [b.stomp({})]
        // return [b.groundSlam({})]
        // return [a.facePlayer({}), b.dash({})]
        // return [b.stampede({})]

        // return []
        return beast.pattern.beastPattern(instance, player, dt)
    }

    /*
     * PATTERNS
     * */

    beast.pattern = {}

    const makeDeltaT = (deltaTime) => (time) => {
        return deltaTime > time*1000
    }

    // BEAST PATTERN
    beast.pattern.beastPattern = (() => {
        // INIT
        let deltaTime = 0,
            changeCount = {cycle1: 0, cycle2: 0},
            waitTime = 0,
            waitTimeClose = 0

        return (instance, player, dt) => {
            deltaTime += dt

            let activity = [],
                changeConst = {cycle1: 2, cycle2: 5}

            let
                deltaT = makeDeltaT(deltaTime),
                resetT = () => {
                    deltaTime = 0
                    deltaT = makeDeltaT(deltaTime)
                },
                change = (cycle) => {
                    changeCount[cycle]++
                    resetT()
                }

            const
                distance = app.game.distance(instance.gfx.x, instance.gfx.y, player.gfx.x, player.gfx.y) - instance.config.radius - player.config.radius,
                diffAngle = instance.face.rotation - player.face.rotation,
                bossRadius = instance.config.radius,
                playerRadius = player.config.radius

            let
                actions = {
                    far: [
                        {name: "dash",
                            actions: [b.dash({})]},
                        {name: "dash",
                            actions: [b.dash({})]},
                        {name: "dash",
                            actions: [b.dash({})]},
                        {name: "stampede",
                            actions: [b.stampede({})]}
                    ],
                    near: [
                        {name: "stomp",
                            actions: [b.stomp({})]},
                        {name: "groundSlam",
                            actions: [b.groundSlam({})]},
                        {name: "groundSlam",
                            actions: [b.groundSlam({})]}
                    ],
                    close: [
                        {name: "stomp",
                            actions: [b.stomp({})]},
                        {name: "hitRound",
                            actions: [b.hitRound({hitCount: 7})]},
                        {name: "hitRound",
                            actions: [b.hitRound({hitCount: 7})]}
                    ]
                }

            if(behaviour.activity.activeAction(instance))
                return []

            if(behaviour.activity.endAction(instance)) {
                resetT()
                waitTime = randomInt(2, 7)
                waitTimeClose = waitTime + randomInt(2, 5)
            }

            activity = [a.facePlayer({})]

            if(deltaT(waitTime)) {
                activity = []

                if(behaviour.tags.has("DISTANCE_CLOSE") && deltaT(waitTimeClose)) {
                    if(distance >= 50)
                        behaviour.tags.remove("DISTANCE_CLOSE")
                }

                if(distance < 50 || (behaviour.tags.has("DISTANCE_CLOSE") && !deltaT(waitTimeClose))) {
                    if(!behaviour.tags.has("DISTANCE_CLOSE")) {
                        behaviour.tags.addPerm("DISTANCE_CLOSE")
                        return []
                    }

                    // activity.push(a.facePlayer({gfx: [instance.face, instance.hpMask]}))

                    if(deltaT(waitTimeClose)) {
                        let i = randomInt(0, actions.close.length-1)
                        activity.push(...actions.close[i].actions)
                        behaviour.tags.remove("DISTANCE_CLOSE")
                    }
                }
                else if(distance < 200) {
                    let i = randomInt(0, actions.near.length-1)
                    activity.push(...actions.near[i].actions)
                }
                else {
                    let i = randomInt(0, actions.far.length-1)
                    activity.push(...actions.far[i].actions)
                }
            }

            return activity
        }
    })()

    behaviour.activity = behaviour.activity || {}
    beast.activity = beast.activity || {}
    let a = behaviour.activity // ALIAS
    let b = beast.activity // ALIAS

    /*
     * ANIMATIONS
     * */

    // beast.activity.braceLimbs = (instance) => {
    //     const anim = (obj, posY = -1) => {
    //         const pos = obj.gfx
    //         let anim = new TimelineMax({})
    //         anim.to(obj.gfx, .25, {scaleX: 1.2, scaleY: 1.2, ease: Power1.easeIn}, 0)
    //         anim.to(obj.gfx, .25, {x: pos.x+5, y: pos.y+(posY*2.5), ease: Power1.easeIn}, 0)
    //         anim.to(obj.gfx, .25, {scaleX: 1, scaleY: 1, ease: Power1.easeOut}, .25)
    //         anim.to(obj.gfx, .25, {x: pos.x+10, y: pos.y+(posY*5), ease: Power1.easeOut}, .25)
    //         return anim
    //     }
    //
    //     let animation = new TimelineMax({})
    //     animation.add(anim(instance.leftLimb), 0)
    //     animation.add(anim(instance.rightLimb, 1), .66)
    // }

    // beast.activity.stompLimb = (instance) => {
    //     const anim = (obj) => {
    //         const
    //             pos = obj.gfx,
    //             halfDuration = .25
    //         let anim = new TimelineMax({})
    //         anim.to(obj.gfx, halfDuration, {scaleX: 1.5, scaleY: 1.5, ease: Power1.easeIn}, 0)
    //         anim.to(obj.gfx, halfDuration, {x: pos.x+10, y: pos.y/2, ease: Power1.easeIn}, 0)
    //         anim.to(obj.gfx, halfDuration, {scaleX: 1, scaleY: 1, ease: Power1.easeOut}, halfDuration)
    //         anim.to(obj.gfx, halfDuration, {x: pos.x+20, y: 0, ease: Power1.easeOut}, halfDuration)
    //         return anim
    //     }
    //
    //     let animation = new TimelineMax({})
    //     animation.add(anim(instance.rightLimb))
    // }

    // beast.activity.resetLimbs = (instance) => {
    //     const
    //         ease = Power3.easeOut,
    //         duration = .33
    //     TweenMax.to(instance.leftLimb.gfx, duration, {alpha: 1, ease})
    //     TweenMax.to(instance.leftLimb.gfx, duration, {scaleX: 1, scaleY: 1, ease})
    //     TweenMax.to(instance.leftLimb.gfx, duration, {x: instance.config.limbPosition, y: -instance.config.limbPosition, ease})
    //     TweenMax.to(instance.rightLimb.gfx, duration, {alpha: 1, ease})
    //     TweenMax.to(instance.rightLimb.gfx, duration, {scaleX: 1, scaleY: 1, ease})
    //     TweenMax.to(instance.rightLimb.gfx, duration, {x: instance.config.limbPosition, y: instance.config.limbPosition, ease})
    // }

    /*
     * ACTIVITIES
     * */

    beast.activity.stampede =
        ({hitReach = 60, hitAngle = .15*Math.PI, hitDamage = 20, rotationAngle = .25*Math.PI, minDistance = 250, speed = 1.2, distanceEaseOut = 75, timeEaseOut = 2, hitTime = .5}) =>
            (instance, player) => {
                if(!behaviour.activity.startAction(instance))
                    return

                boss.rotate(instance, app.game.getAngle(instance.gfx, player.gfx))

                const
                    rotation = toRad(instance.face.rotation),
                    distance = Math.max(minDistance, app.game.distance(instance.gfx.x, instance.gfx.y, player.gfx.x, player.gfx.y)),
                    angle = app.game.getAngle(instance.gfx, player.gfx),
                    xFinish = instance.gfx.x + Math.cos(angle) * distance,
                    yFinish = instance.gfx.y + Math.sin(angle) * distance,
                    time = distance / speed / 100

                distanceEaseOut += distance
                const
                    xEaseOut = instance.gfx.x + Math.cos(angle) * distanceEaseOut,
                    yEaseOut = instance.gfx.y + Math.sin(angle) * distanceEaseOut

                let timeline = new TimelineMax({
                    onComplete: () => {
                        setTimeout(() => {
                            behaviour.activity.stopAction(instance)
                        }, 500)
                    }
                })

                timeline.to(instance.gfx, time, {x: xFinish, y: yFinish, ease: Power0.easeNone}, .5)
                timeline.to(instance.gfx, timeEaseOut, {x: xEaseOut, y: yEaseOut, ease: Power3.easeOut})

                // ROTATING & HITTING
                const
                    numberOfHits = Math.floor((time+(timeEaseOut/2)) / hitTime)

                let hitCount = 0
                const hit = () => {
                    let rotateTo = rotation + randomInt(0, (hitCount%2 === 0 ? rotationAngle : -rotationAngle)*100)/100
                    TweenMax.to([instance.face/*, instance.limbs*/], hitTime, {rotation: toDegree(rotateTo), ease: Power1.easeInOut, delay: hitCount*hitTime, onComplete: () => {
                        a.hitArc(hitReach, hitAngle, hitDamage, {})(instance)
                    }})

                    hitCount++
                    if(hitCount < numberOfHits)
                        hit()
                }
                hit()
            }

    beast.activity.dash =
        ({hitReach = 8, hitDamage = 30, destinationAngleSpread = .05*Math.PI}) =>
            (instance, player) => {
                if(!behaviour.activity.startAction(instance))
                    return

                const
                    angle = app.game.getAngle(instance.gfx, player.gfx) + randomInt(-destinationAngleSpread*100, destinationAngleSpread*100)/100,
                    drawAngle = Math.PI + angle,
                    drawDistance = 20,
                    brakeDistance = instance.config.radius*2

                let timeline = new TimelineMax({onComplete: () => {
                    setTimeout(() => {
                        behaviour.activity.stopAction(instance)
                    }, 500)
                }})

                const
                    xDraw = instance.gfx.x + Math.cos(drawAngle)*drawDistance,
                    yDraw = instance.gfx.y + Math.sin(drawAngle)*drawDistance
                timeline.to(instance.gfx, 1, {x: xDraw, y: yDraw, ease: Power3.easeOut})

                const
                    distance = brakeDistance + app.game.distance(instance.gfx.x, instance.gfx.y, player.gfx.x, player.gfx.y),
                    xFinish = instance.gfx.x + Math.cos(angle) * distance,
                    yFinish = instance.gfx.y + Math.sin(angle) * distance,
                    electricReach = 8,
                    electricRoughness = 3,
                    electricThickness = 2,
                    hitRadius = instance.config.radius + electricReach + electricRoughness + (electricThickness/2),
                    hitting = () => {
                        app.fx.electricArc(instance, electricReach, "circle", "dash", {parent: instance.gfx, roughness: electricRoughness, thickness: electricThickness})

                        if(instance.behaviour.tags.has("DASH_PLAYER_HIT"))
                            return

                        let playerHits = app.game.collisionCircles(instance.gfx.x, instance.gfx.y, hitRadius)
                            .filter(c => c.gameObject.isPlayer)

                        if(playerHits.length) {
                            instance.behaviour.tags.addPerm("DASH_PLAYER_HIT")
                            playerHits.forEach(playerHit => {
                                let angle = app.game.getAngle(instance.gfx, playerHit.gameObject.gfx),
                                    length = 5,
                                    force = app.game.polarToCartesian(length, angle)
                                playerHit.gameObject.applyForce(force)
                                playerHit.gameObject.takeDamage(hitDamage,
                                    app.game.getHitPosition(playerHit.intersections, playerHit.gameObject))
                            })
                        }
                    },
                    finish = () => {
                        app.fx.electricArcDelete(instance, "dash", instance.gfx)
                        instance.behaviour.tags.remove("DASH_PLAYER_HIT")
                    }
                timeline.to(instance.gfx, 1, {x: xFinish, y: yFinish, ease: Power2.easeOut, onUpdate: hitting, onComplete: finish}, "+=.25")
            }

    beast.activity.groundSlam =
        ({sectionReach = 50, sections = 5, minDamage = 10, maxDamage = 20, minHitAngle = .1*Math.PI, maxHitAngle = .1*Math.PI, time = 1}) =>
            (instance) => {
                if(!behaviour.activity.startAction(instance))
                    return

                let sectionsCount = 0,
                    arcAnimationTime = 1
                time = (time !== "serial") ? time : (arcAnimationTime*.75)*sections
                const hit = () => {
                    const
                        ratio = sectionsCount/(sections-1),
                        dmg = (ratio * (maxDamage-minDamage))+minDamage,
                        hitAngle = (1-ratio) * (maxHitAngle-minHitAngle)+minHitAngle

                    a.hitArc(sectionReach, hitAngle, dmg, {radiusAdjust: sectionsCount*sectionReach, animationTime: arcAnimationTime})(instance)

                    sectionsCount++
                    if(sectionsCount < sections)
                        setTimeout(hit, time/sections*1000)
                    else
                        setTimeout(() => behaviour.activity.stopAction(instance), 500)
                }
                // b.stompLimb(instance)
                setTimeout(() => {
                    // setTimeout(() => b.resetLimbs(instance), 500)
                    hit()
                }, 500)
            }

    beast.activity.stomp =
        ({hitCloseDamage = 50, hitCloseReach = 25, hitWideDamage = 10, hitWideReach = 75}) =>
            (instance) => {
                if(!behaviour.activity.startAction(instance))
                    return

                let timeline = new TimelineMax()

                timeline.to([instance.body, instance.face, instance.hpBar], 1.25, {scaleX: 1.33, scaleY: 1.33, ease: Power2.easeOut}, 0)
                timeline.to([instance.body, instance.hpBar], 1.25, {alpha: .75, ease: Power2.easeOut}, 0)

                timeline.to([instance.body, instance.face, instance.hpBar], .1, {scaleX: 1, scaleY: 1, ease: Power2.easeOut}, 1.25)
                timeline.to([instance.body, instance.hpBar], .1, {alpha: 1, ease: Power2.easeOut}, 1.25)

                timeline.addCallback(() => {
                    a.hitCircle(hitCloseReach, hitCloseDamage, {})(instance)
                    a.hitCircle(hitWideReach, hitWideDamage, {})(instance)
                    setTimeout(() => behaviour.activity.stopAction(instance), 500)
                }, "-=0")
            }

    beast.activity.hitRound =
        ({hitDamage = 10, hitReach = 35, hitAngle = .1*Math.PI, time = 2, hitCount = null}) =>
            (instance) => {
                if(!behaviour.activity.startAction(instance))
                    return

                let
                    angle = hitAngle*2,
                    rotationDirection = toDegree(randomInt(0, 1) === 0 ? Math.PI*2 : -Math.PI*2),
                    rotationStart = instance.face.rotation,
                    rotationEnd = rotationStart+rotationDirection
                hitCount = hitCount ? hitCount : (2*Math.PI / angle)

                generateArr(0, hitCount-1)
                    .forEach(i =>
                        setTimeout(
                            () => a.hitArc(hitReach, hitAngle, hitDamage, {})(instance),
                            Linear.easeNone.getRatio(i/hitCount)*time*1000))

                const onUpdate = function() {
                    const currentRotation = this.ratio*(rotationEnd-rotationStart)+rotationStart
                    boss.rotate(instance, currentRotation, true)
                }

                const onComplete = () => {
                    boss.rotate(instance, rad(toRad(instance.face.rotation)))
                    setTimeout(() => behaviour.activity.stopAction(instance), 500)
                }

                TweenMax.to(instance.face, time, {onUpdate, rotationDummy: rotationEnd, ease: Linear.easeNone, onComplete})
            }

    beast.init = (instance) => {
        Object.assign(beast.activity, behaviour.activity)

        behaviour.activity.stopAction = ((stopAction) => (instance) => {
            stopAction(instance)
            // beast.activity.resetLimbs(instance)
        })(behaviour.activity.stopAction)

        boss.rotate = boss.rotate([instance.face/*, instance.hpBar, instance.limbs*/])
    }

    /*
     * END
     * */
    return beast
})(window.app.boss.behaviour.beast, window.app.boss.behaviour, window.app.boss, window.app, createjs, TweenMax, EasingFunctions)