"use strict"

window.app = window.app || {}
window.app.boss = window.app.boss || {}
window.app.boss.behaviour = window.app.boss.behaviour || {}
window.app.boss.behaviour.beast = window.app.boss.behaviour.beast || {}

/*
 * DOUBLE RANDOM
 * */
window.app.boss = ((boss, app, $, createjs, TweenMax, EasingFunctions) => {

    let configStandart = (() => {
        // const
        //     radius = 40,
        //     strokeWidth = 3,
        //     faceWidth = 14,
        //     eyeWidth = faceWidth - strokeWidth*2 - 2,
        //     limbPosition = radius*(5/9),
        //     limbRadius = radius/3,
        //     hpRadius = radius-strokeWidth/2,
        //     hpWidth = hpRadius*2 - faceWidth + strokeWidth/2 + .5

        /*
        * GFX
        * */
        const
            radius = 40,
            cacheRadius = radius*1.1,
            outerBodyWidth = radius/3-1,
            outerBodyDistance = radius - (outerBodyWidth/2) + .5,
            bodyRadius = radius - outerBodyWidth + 1,
            innerBodyRadius = radius/6,
            innerBodyIndicatorRadius = innerBodyRadius*.5,

            bodyBridgeWidth = innerBodyRadius*.75,
            bodyBridgeHeight = bodyRadius*2,
            bodyBridgeX = -bodyBridgeWidth/2,
            bodyBridgeY = -bodyBridgeHeight/2,

            borderWidth = 3,
            faceAngle = Math.PI*.25

        return {
            type: "beast",

            // borderColor: "#4c423a",
            // contentColor: "#30261C",
            // contentColorSecondary: "#cbf2f2",
            // faceColor: "#1F5F61",
            // eyeColor: "#0B8185",
            // hitColor: "#0B8185",
            // backgroundColor: "#6D7D41",

            // borderColor: "#eee",
            // contentColor: "#eee",
            // contentColorSecondary: "#000",
            // faceColor: "#000",
            // eyeColor: "#000",
            // hitColor: "#db0b0b",
            
            // COLORS
            backgroundColor: "#000",
            borderColor: "#aaa",
            bodyColor: "#eee",
            innerBodyColor: "#eee",
            innerBodyIndicatorColor: "#06f",
            outerBodyColor: "#eee",
            borderHpColor: "#f33",
            faceColor: "#00DFFC",
            hitColor: "#00DFFC",
            specialHitColor: "#06f",

            // radius,
            // strokeWidth,
            //
            // limbPosition,
            // limbRadius,
            // hpRadius,
            // hpWidth,
            //
            // faceWidth,
            // eyeWidth,
            // faceAngle: Math.PI*.25,

            // GFX
            radius,
            cacheRadius,
            bodyRadius,
            innerBodyRadius,
            innerBodyIndicatorRadius,

            bodyBridgeWidth,
            bodyBridgeHeight,
            bodyBridgeX,
            bodyBridgeY,

            outerBodyDistance,
            outerBodyWidth,
            borderWidth,
            faceAngle,

            hp: 100,
            speed: 2,
        }
    })()

    boss.create = () => {
        let instance = {}

        instance.config = configStandart
        instance.ID = ID()
        instance.hasBody = true
        instance.isBoss = true

        instance.g = {} // NAMESPACE OF USABLE GFX OBJECTS
        instance.fx = {} // NAMESPACE OF USABLE FX OBJECTS

        const type = boss.behaviour[instance.config.type]

        ;({radius} = instance.config)

        /*
        * GFX
        * BODY
        * */
        instance.gfx = new createjs.Container()
        instance.gfx.z = 11
        app.stage.addChild(instance.gfx)

        // let body = new createjs.Shape()
        // body.graphics
        //     .f(instance.config.contentColorSecondary)
        //     .dc(0, 0, instance.config.radius)
        // body.z = 9
        // instance.gfx.addChild(body)
        // instance.body = body
        //
        // // LIMBS
        // instance.limbs = new createjs.Container()
        // instance.limbs.z = 5
        // instance.gfx.addChild(instance.limbs)
        //
        // let leftLimb = new createjs.Shape()
        // leftLimb.graphics
        //     .f(instance.config.contentColorSecondary)
        //     .ss(instance.config.strokeWidth)
        //     .s(instance.config.borderColor)
        //     .dc(0, 0, instance.config.limbRadius)
        // leftLimb.x = instance.config.limbPosition
        // leftLimb.y = -instance.config.limbPosition
        // instance.limbs.addChild(leftLimb)
        // instance.leftLimb = {
        //     gfx: leftLimb,
        //     hasBody: true,
        //     config: {
        //         radius: instance.config.limbRadius
        //     }
        // }
        //
        // let rightLimb = new createjs.Shape()
        // rightLimb.graphics
        //     .f(instance.config.contentColorSecondary)
        //     .ss(instance.config.strokeWidth)
        //     .s(instance.config.borderColor)
        //     .dc(0, 0, instance.config.limbRadius)
        // rightLimb.x = instance.config.limbPosition
        // rightLimb.y = instance.config.limbPosition
        // instance.limbs.addChild(rightLimb)
        // instance.rightLimb = {
        //     gfx: rightLimb,
        //     hasBody: true,
        //     config: {
        //         radius: instance.config.limbRadius
        //     }
        // }
        //
        //
        // let hpBar = new createjs.Container()
        // hpBar.z = 15
        // instance.gfx.addChild(hpBar)
        // instance.hpBar = hpBar
        //
        // let hpMask = new createjs.Shape()
        // hpMask.graphics
        //     .dr(-instance.config.hpRadius, -instance.config.hpRadius, instance.config.hpWidth, instance.config.hpRadius*2)
        // instance.hpBar.addChild(hpMask)
        // instance.hpMask = hpMask
        //
        // let hp = new createjs.Shape()
        // hp.graphics
        //     .f(instance.config.contentColor)
        //     .dc(0, 0, instance.config.hpRadius)
        // hp.mask = hpMask
        // instance.hpBar.addChild(hp)
        //
        // /*
        // * FACE
        // * */
        // let faceCont = new createjs.Container()
        // faceCont.z = 19
        // instance.gfx.addChild(faceCont)
        // instance.face = faceCont
        //
        // let
        //     faceDistance = instance.config.radius - instance.config.faceWidth/2,
        //     faceColor = instance.config.faceColor,
        //     eyeWidth = instance.config.eyeWidth,
        //     eyeColor = instance.config.eyeColor,
        //     eyeRatio = .5
        //
        // let faceBorder = new createjs.Shape()
        // faceBorder.graphics
        //     .f(instance.config.borderColor)
        //     .dr(0, 0, 15, 15)
        // faceBorder.x = 18
        // faceBorder.y = -radius + 6
        // faceBorder.mask = body
        // instance.face.addChild(faceBorder)
        //
        // let faceBorder2 = new createjs.Shape()
        // faceBorder2.graphics
        //     .f(instance.config.borderColor)
        //     .dr(0, 0, 15, 15)
        // faceBorder2.x = 18
        // faceBorder2.y = radius - 21
        // faceBorder2.mask = body
        // instance.face.addChild(faceBorder2)
        //
        // let faceBorderMain = new createjs.Shape()
        // faceBorderMain.graphics
        //     .ss(instance.config.faceWidth)
        //     .s(instance.config.borderColor)
        //     .arc(0, 0, faceDistance, -instance.config.faceAngle, instance.config.faceAngle)
        // instance.face.addChild(faceBorderMain)
        //
        // let faceFill = new createjs.Shape()
        // faceFill.graphics
        //     .ss(eyeWidth)
        //     .s(faceColor)
        //     .arc(0, 0, faceDistance, -instance.config.faceAngle+.1, instance.config.faceAngle-.1)
        // instance.face.addChild(faceFill)
        //
        // let eye = new createjs.Shape()
        // eye.graphics
        //     .ss(eyeWidth)
        //     .s(eyeColor)
        //     .arc(0, 0, faceDistance, -(eyeRatio*(instance.config.faceAngle-.09)), eyeRatio*(instance.config.faceAngle-.09))
        // instance.face.addChild(eye)
        // instance.eye = eye

        let c = instance.config
            ;({radius, bodyRadius, innerBodyRadius, innerBodyIndicatorRadius, outerBodyDistance, outerBodyWidth, borderWidth, borderColor, bodyColor, innerBodyColor, innerBodyIndicatorColor, outerBodyColor, faceColor, faceAngle} = instance.config)

        /*
         * FACE
         * */
        let faceCont = new createjs.Container()
        faceCont.z = 19
        instance.gfx.addChild(faceCont)
        instance.face = faceCont

        let bodyBridge = new createjs.Shape()
        bodyBridge.graphics
            .f(innerBodyColor)
            .dr(0, 0, c.bodyBridgeWidth, c.bodyBridgeHeight)
        bodyBridge.x = c.bodyBridgeX
        bodyBridge.y = c.bodyBridgeY
        instance.face.addChild(bodyBridge)

        let innerBody = new createjs.Shape()
        innerBody.graphics
            .f(innerBodyColor)
            .dc(0, 0, innerBodyRadius)
        instance.face.addChild(innerBody)

        let innerBodyIndicator = new createjs.Shape()
        innerBodyIndicator.graphics
            .f(innerBodyIndicatorColor)
            .dc(0, 0, innerBodyIndicatorRadius)
        instance.face.addChild(innerBodyIndicator)
        instance.g.innerBodyIndicator = innerBodyIndicator

        let outerBody = new createjs.Shape()
        outerBody.graphics
            .ss(outerBodyWidth)
            .s(outerBodyColor)
            .arc(0, 0, outerBodyDistance, faceAngle, -faceAngle)
        instance.face.addChild(outerBody)

        let border = new createjs.Shape()
        border.graphics
            .ss(borderWidth)
            .s(borderColor)
            .arc(0, 0, outerBodyDistance, faceAngle, -faceAngle)
        instance.face.addChild(border)

        let hpBorder = new createjs.Shape()
        hpBorder.graphics
            .ss(borderWidth)
            .s(c.borderHpColor)
            .arc(0, 0, outerBodyDistance, faceAngle, -faceAngle)
        instance.face.addChild(hpBorder)
        instance.g.hpBorder = hpBorder

        let faceTop = new createjs.Shape()
        faceTop.graphics
            .ss(borderWidth)
            .s(faceColor)
            .arc(0, 0, outerBodyDistance, -faceAngle, 0)
        instance.face.addChild(faceTop)
        instance.g.faceTop = faceTop

        let faceBottom = new createjs.Shape()
        faceBottom.graphics
            .ss(borderWidth)
            .s(faceColor)
            .arc(0, 0, outerBodyDistance, faceAngle, 0, true)
        instance.face.addChild(faceBottom)
        instance.g.faceBottom = faceBottom

        instance.face.cache(-c.cacheRadius, -c.cacheRadius, c.cacheRadius*2, c.cacheRadius*2)

        // MOVE & ROTATE
        app.game.moveTo(instance.gfx, app.canvas_width/3, app.canvas_height/3)
        boss.rotate(instance, 45, true)
        app.game.sortGfx(instance.gfx)

        // GAMEPLAY
        instance.maxHp = instance.config.hp
        instance.hp = instance.config.hp
        instance.takeDamage = takeDamage(instance)
        instance.behaviour = {
            update: type.update,
            activity: [],
            tags: boss.behaviour.tags
        }

        type.init(instance)

        app.stage.canvas.style.backgroundColor = instance.config.backgroundColor

        return instance
    }

    boss.destroy = (instance) => {
        if(instance.destroyed)
            return

        instance.destroyed = true
        TweenMax.to(instance.gfx, .25, {alpha: 0, delay: .25})
        TweenMax.to(instance.gfx, .5, {scaleX: 0, scaleY: 0, ease: Back.easeIn, onComplete:()=>{
            instance.gfx.radius = 0
            instance.gfx.x = -99999
            instance.gfx.y = -99999
        }})
    }

    const takeDamage = (instance) => (damage, hitPosition) => {
        instance.hp -= damage
        app.fx.getHit(instance, hitPosition)

        if(instance.hp <= 0)
            boss.destroy(instance)
    }

    boss.rotate = (gfxCurry) => (instance, rotation, isDegree = false, gfx = false) => {
        if(!isDegree)
            rotation = toDegree(rotation)

        gfx = !gfx ? gfxCurry : gfx

        gfx.forEach(g => g.rotation = rotation)
    }

    /*
    * BOSS
    * UPDATE
    * */
    boss.update = (instance, player, deltaTime) => {
        if(instance.destroyed || player.destroyed)
            return

        instance.behaviour.activity = boss.behaviour.update(instance, player, deltaTime)
        instance.behaviour.tags.clear()
        instance.behaviour.activity.forEach(a => a(instance, player, deltaTime))
    }

    // app.addInitCallback(player.init)

    /*
     * END
     * */
    return boss
})(window.app.boss, window.app, jQuery, createjs, TweenMax, EasingFunctions)