"use strict"

window.app = window.app || {}
window.app.fx = window.app.fx || {}

/*
 * EFFECTS
 * */
window.app.fx = ((fx, app, $, createjs, TweenMax, EasingFunctions) => {
    
    fx.getHit = (instance, hitPosition) => {
        const
            durationFrom = .1,
            durationTo = .5

        let filter = new createjs.ColorFilter(1, 1, 1, 1, 0, 0, 0)
        instance.face.filters = [filter]

        let timeline = new TimelineMax()

        timeline.to(filter, durationFrom, {redMultiplier: 0, blueMultiplier: 0, greenMultiplier: 0, redOffset: 255, ease: Power2.easeIn}, 0)
        timeline.to(filter, durationTo, {redMultiplier: 1, blueMultiplier: 1, greenMultiplier: 1, redOffset: 0, ease: Power2.easeOut, onComplete: () => {
            instance.face.filters = []
        }}, durationFrom)

        // SPARKS
        const throwParticle = i => {
            const
                sparkTypeLong = randomInt(0, 3) === 0,

                duration = randomInt(6, 8)/10,
                startChange = duration/5,
                changeDuration = duration/3,
                startFade = duration/4,
                fadeDuration = duration - startChange - duration/3,

                slowDuration = duration,
                slowFadeDuration = duration*.25,

                colors = [
                    {r: 239, g: 19, b: 29, hex: "#ef131d"},
                    {r: 237, g: 30, b: 30, hex: "#ed1e1e"},
                    {r: 191, g: 11, b: 11, hex: "#bf0b0b"},
                    {r: 183, g: 3, b: 3, hex: "#b70303"}
                ],
                color = colors.splice(randomInt(0, colors.length-1), 1)[0],
                colorFilter = new createjs.ColorFilter(0, 0, 0, 1, 255, 215, 96, 0),

                maxRadiusFixed = 20,
                maxRadius = maxRadiusFixed/10,
                radius = randomInt(10, maxRadiusFixed)/10,
                startRadius = 1,

                direction = app.game.getAngle(instance.gfx, hitPosition),
                angle = direction + randomInt(0, 40)/100*Math.PI * ((round(Math.random(), 0) < 1) ? -1 : 1),
                distance = randomInt(30, 50),
                x = hitPosition.x + (distance * Math.cos(angle)),
                y = hitPosition.y + (distance * Math.sin(angle))

            let spark = new createjs.Shape()
            spark.graphics
                .f(color.hex)
                .dc(0, 0, startRadius)
            spark.x = hitPosition.x
            spark.y = hitPosition.y
            spark.filters = [colorFilter]
            spark.cache(-maxRadius, -maxRadius, maxRadius*2, maxRadius*2)
            app.stage.addChild(spark)

            let timeline = new TimelineMax({
                onUpdate: () => spark.updateCache(),
                onComplete: () => app.stage.removeChild(spark)
            })
            if(sparkTypeLong) {
                timeline.to(spark, duration, {x, y, ease: Power3.easeOut})
                timeline.to(spark, fadeDuration, {alpha: 0, ease: Power1.easeIn}, startFade)
                timeline.to(spark.graphics.command, changeDuration, {radius, ease: Power3.easeOut}, startChange)
                timeline.to(colorFilter, changeDuration,
                    {redMultiplier: 1, greenMultiplier: 1, blueMultiplier: 1, redOffset: color.r, greenOffset: color.g, blueOffset: color.b, ease: Power3.easeOut}, startFade)
            }
            else {
                timeline.to(spark, slowDuration, {x, y, ease: Power3.easeOut})
                timeline.to(spark, slowFadeDuration, {alpha: 0, ease: Power1.easeIn}, 0)
            }
        }

        if(hitPosition)
            generateArr(1, 50)
                .forEach(throwParticle)
    }

    fx.hitArc = (instance, reach, angle, {parent = app.stage, animationTime = .5, rotationSet = false, rotationAdjust = 0, radiusAdjust = 0, color = false}) =>
        new Promise((resolve, reject) => {
            const
                isCircle = angle === "circle"

            const
                rotation = (rotationSet === false) ?
                    toRad(instance.face.rotation) + rotationAdjust :
                    rotationSet,
                radius = instance.config.radius + radiusAdjust,
                x = (parent === app.stage) ? instance.gfx.x : 0,
                y = (parent === app.stage) ? instance.gfx.y : 0,
                hitColor = (color === false) ? instance.config.hitColor : color

            const
                hitStrokeWidth = 2,
                arcStartPos = radius + (hitStrokeWidth / 2),
                arcEndPos = radius + reach,
                areaMiddlePos = radius + (reach * .75),
                areaEndPos = radius + reach + (hitStrokeWidth / 2),
                fadeTime = .25,
                hitAreaMaxStroke = reach * .5,
                ease = Power2.easeOut,
                fadeEase = Power2.easeOut

            let hitArc = new createjs.Shape()
            hitArc.graphics
                .s(hitColor)
                .ss(hitStrokeWidth)
            if(isCircle)
                hitArc.graphics.dc(0, 0, arcStartPos)
            else
                hitArc.graphics.arc(0, 0, arcStartPos, -angle, angle)
            hitArc.x = x
            hitArc.y = y
            hitArc.z = 3
            hitArc.rotation = toDegree(rotation)
            parent.addChild(hitArc)

            let hitArea = new createjs.Shape(),
                strokeWidth = hitArea.graphics
                    .s(hitColor)
                    .ss(hitStrokeWidth).command
            if(isCircle)
                hitArea.graphics.dc(0, 0, arcStartPos)
            else
                hitArea.graphics.arc(0, 0, arcStartPos, -angle, angle)
            hitArea.x = x
            hitArea.y = y
            hitArea.z = 3
            hitArea.rotation = toDegree(rotation)
            hitArea.alpha = .2
            parent.addChild(hitArea)

            app.game.sortGfx(parent)

            // COLLISION
            const onUpdate = () => {
                let hits = isCircle ?
                    app.game.collisionCircles(hitArc.x, hitArc.y, hitArc.graphics.command.radius, instance.ID) :
                    app.game.collisionArcToCircles(hitArc.x, hitArc.y, hitArc.graphics.command.radius, rotation, angle, instance.ID)

                hits = hits.filter(c => c.gameObject.hp)

                if(hits.length) {
                    timeline.pause()
                    TweenMax.to(hitArc, fadeTime, {alpha: 0, ease: fadeEase})
                    TweenMax.to(hitArea, fadeTime, {alpha: 0, ease: fadeEase})

                    resolve({type: "hit", hits})
                }
            }

            let timeline = new TimelineMax({
                onComplete: () => {
                    parent.removeChild(hitArea, hitArc)
                    resolve({type: "animationEnd"})
                }
            })
            timeline.to(hitArc.graphics.command, animationTime, {radius: arcEndPos, ease, onUpdate})
            timeline.to(hitArc, fadeTime, {alpha: 0, ease: fadeEase}, animationTime*.5)

            timeline.to(strokeWidth, animationTime, {width: hitAreaMaxStroke, ease}, 0)
            timeline.to(hitArea.graphics.command, animationTime, {radius: areaMiddlePos, ease}, 0)

            timeline.to(strokeWidth, animationTime*.5, {width: 0, ease}, animationTime*.5)
            timeline.to(hitArea.graphics.command, animationTime*.5, {radius: areaEndPos, ease}, animationTime*.5)
        })

    fx.movementTrail = (instance, {parent = app.stage, rotationSet = false, rotationAdjust = 0, radiusAdjust = 0, color = false}) => {
        let change

        change = !instance.fx["trail"] ? true :
            app.game.time - instance.fx["trail"].changeTime > 10

        change = change && (instance.movement.centripetal !== 0 || instance.movement.radial !== 0)

        if(!change)
            return

        if(!instance.fx["trail"])
            instance.fx["trail"] = {}

        instance.fx["trail"].changeTime = app.game.time

        const
            radius = instance.config.radius + radiusAdjust,
            x = (parent === app.stage) ? instance.gfx.x : 0,
            y = (parent === app.stage) ? instance.gfx.y : 0

        let trailOuter = new createjs.Shape()
        trailOuter.graphics
            .f("#ef131d")
            .dc(0, 0, radius)
        trailOuter.x = x
        trailOuter.y = y
        trailOuter.z = 1
        trailOuter.alpha = .02
        parent.addChild(trailOuter)

        app.game.sortGfx(parent)

        TweenMax.to(trailOuter, 2, {alpha: 0})
        TweenMax.to(trailOuter.graphics.command, 2, {radius: radius*3, ease: Power2.easeOut, onComplete: ()=>parent.removeChild(trailOuter)})

        let trailInner = new createjs.Shape()
        trailInner.graphics
            .f("rgb(255, 215, 96)")
            .dc(0, 0, radius)
        trailInner.x = x
        trailInner.y = y
        trailInner.z = 2
        trailInner.alpha = .05
        parent.addChild(trailInner)

        app.game.sortGfx(parent)

        TweenMax.to(trailInner, 1.5, {alpha: 0, ease: Power2.easeOut})
        TweenMax.to(trailInner.graphics.command, 1.5, {radius: 0, ease: Power2.easeOut, onComplete: ()=>parent.removeChild(trailInner)})
    }

    fx.electricArc = (instance, reach, angle, name,
        {parent = app.stage, roughness = 2, thickness = 2, rotationSet = false, rotationAdjust = 0, radiusAdjust = 0, color = false}) => {
        const isCircle = angle === "circle"

        const change = !instance.fx[name] ?
            true :
            app.game.time - instance.fx[name].changeTime > 100

        if(!change)
            return

        if(!instance.fx[name])
            instance.fx[name] = {}

        instance.fx[name].changeTime = app.game.time
        parent.removeChild(instance.fx[name].fx)

        const
            rotation = (rotationSet === false) ?
            toRad(instance.face.rotation) + rotationAdjust :
                rotationSet,
            radius = instance.config.radius + radiusAdjust,
            x = (parent === app.stage) ? instance.gfx.x : 0,
            y = (parent === app.stage) ? instance.gfx.y : 0,
            electricColor = (color === false) ? instance.config.hitColor : color

        const
            reachRadius = radius + reach,
            lineLengthPx = 2

        const lineTo = angle => {
            let l = randomInt(-roughness, roughness) + reachRadius,
                coords = app.game.polarToCartesian(l, angle)
            electric.graphics
                .lt(coords.x, coords.y)
        }

        const draw = (electric, startAngle, endAngle, lineLengthAngle, startPosition) => {
            electric.graphics
                .s(electricColor)
                .ss(thickness)
                .mt(startPosition.x, startPosition.y)
            electric.x = x
            electric.y = y
            electric.z = 3

            for(let angle = startAngle; angle <= endAngle; angle += lineLengthAngle) {
                lineTo(angle)
            }
            lineTo(endAngle)
        }

        let electric = new createjs.Shape()

        if(isCircle) {
            const
                startAngle = 0,
                endAngle = 2*Math.PI,
                lineLengthAngle = lineLengthPx / reachRadius,
                startPosition = app.game.polarToCartesian(reachRadius, startAngle)

            draw(electric, startAngle, endAngle, lineLengthAngle, startPosition)
        }
        else {
            const
                startAngle = rotation - angle,
                endAngle = rotation + angle,
                lineLengthAngle = lineLengthPx / reachRadius,
                startPosition = app.game.polarToCartesian(reachRadius, startAngle)

            draw(electric, startAngle, endAngle, lineLengthAngle, startPosition)
        }

        parent.addChild(electric)
        app.game.sortGfx(parent)
        instance.fx[name].fx = electric
    }

    fx.electricArcDelete = (instance, name, parent = app.stage) => {
        parent.removeChild(instance.fx[name].fx)
        delete instance.fx[name]
    }

    /*
     * END
     * */
    return fx
})(window.app.fx, window.app, jQuery, createjs, TweenMax, EasingFunctions)