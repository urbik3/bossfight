"use strict"

window.app = window.app || {}
window.app.player = window.app.player || {}

/*
 * DOUBLE RANDOM
 * */
window.app.player = ((player, app, $, createjs, TweenMax, EasingFunctions) => {

    let configRogue = (() => {
        let acceleration = 1,
            drag = acceleration/1.5,
            maxSpeed = 3.5

        return {
            acceleration,
            drag,
            maxSpeed,

            color: 0x99dd33,
            radius: 12.5,

            faceWidth: 3,
            faceColor: 0x559911,

            hp: 100,

            hitReach: 20,
            hitAngle: .2*Math.PI,
            hitDamage: 1,
            hitCooldown: .75
        }
    })()

    let configMage = (() => {
        let acceleration = .1,
            drag = acceleration/1.15,
            maxSpeed = 1

        return {
            acceleration,
            drag,
            maxSpeed,

            color: 0x6699FF,
            radius: 10,

            faceWidth: 3,
            faceColor: 0x114477,

            hp: 100,

            hitReach: 15,
            hitAngle: .2*Math.PI,
            hitDamage: 1,
            hitCooldown: .75
        }
    })()

    let configFighter = (() => {
        let acceleration = .25,
            drag = acceleration/2,
            maxSpeed = 2

        const specials = {
            hit: {
                hitSpecialDamage: 10,
                hitSpecialTime: 1,
                hitSpecialCooldown: 8
            },
            leap: {
                leapSpecialLength: 80,
                leapSpecialTime: .33,
                leapSpecialCooldown: 8
            }
        }

        const
            hp = 100,
            hitReach = 40,
            hitAngle = .15*Math.PI,
            hitDamage = 1,
            hitCooldown = .8

        /*
        * GFX
        * */
        const
            radius = 20,
            cacheRadius = radius*1.1,
            outerBodyWidth = radius/2-1,
            outerBodyDistance = radius - (outerBodyWidth/2) + .5,
            bodyRadius = radius - outerBodyWidth + 1,
            innerBodyRadius = radius/5,
            innerBodyIndicatorRadius = innerBodyRadius*.5,

            bodyBridgeWidth = innerBodyRadius*.75,
            bodyBridgeHeight = bodyRadius*2,
            bodyBridgeX = -bodyBridgeWidth/2,
            bodyBridgeY = -bodyBridgeHeight/2,

            borderWidth = radius/10,
            faceAngle = Math.PI*.25

        const
            hitRadiusAdjust = -outerBodyWidth/2,
            specialHitRadiusAdjust = -(radius - innerBodyRadius),
            hitWidth = hitReach - hitRadiusAdjust,
            specialHitWidth = hitReach - specialHitRadiusAdjust

        let obj = {
            // COLORS
            borderColor: "#aaa",
            bodyColor: "#eee",
            innerBodyColor: "#eee",
            innerBodyIndicatorColor: "#06f",
            outerBodyColor: "#eee",
            borderHpColor: "#f33",
            faceColor: "#00DFFC",
            hitColor: "#00DFFC",
            specialHitColor: "#06f",

            // GFX
            radius,
            cacheRadius,
            bodyRadius,
            innerBodyRadius,
            innerBodyIndicatorRadius,

            bodyBridgeWidth,
            bodyBridgeHeight,
            bodyBridgeX,
            bodyBridgeY,

            outerBodyDistance,
            outerBodyWidth,
            borderWidth,
            faceAngle,

            // OTHER
            acceleration,
            drag,
            maxSpeed,

            hitRadiusAdjust,
            specialHitRadiusAdjust,
            hitWidth,
            specialHitWidth,

            hp,
            hitReach,
            hitAngle,
            hitDamage,
            hitCooldown,

            specialCooldown: specials.hit.hitSpecialCooldown
        }

        Object.assign(obj, specials.hit)

        return obj
    })()

    player.create = () => {
        let instance = {}

        instance.config = configFighter
        instance.ID = ID()
        instance.hasBody = true
        instance.isPlayer = true

        instance.updatedAccelerationX = false
        instance.updatedAccelerationY = false
        instance.movement = {
            centripetal: 0,
            radial: 0,
            force: {x: 0, y: 0},
            vector: {x: 0, y: 0}
        }

        instance.g = {} // NAMESPACE OF USABLE GFX OBJECTS
        instance.fx = {} // NAMESPACE OF USABLE FX OBJECTS
        instance.gfx = new createjs.Container()
        instance.gfx.z = 10
        app.stage.addChild(instance.gfx)

        let c = instance.config
        ;({radius, bodyRadius, innerBodyRadius, innerBodyIndicatorRadius, outerBodyDistance, outerBodyWidth, borderWidth, borderColor, bodyColor, innerBodyColor, innerBodyIndicatorColor, outerBodyColor, faceColor, faceAngle} = instance.config)

        /*
         * FACE
         * */
        let faceCont = new createjs.Container()
        faceCont.z = 19
        instance.gfx.addChild(faceCont)
        instance.face = faceCont

        let bodyBridge = new createjs.Shape()
        bodyBridge.graphics
            .f(innerBodyColor)
            .dr(0, 0, c.bodyBridgeWidth, c.bodyBridgeHeight)
        bodyBridge.x = c.bodyBridgeX
        bodyBridge.y = c.bodyBridgeY
        instance.face.addChild(bodyBridge)

        let innerBody = new createjs.Shape()
        innerBody.graphics
            .f(innerBodyColor)
            .dc(0, 0, innerBodyRadius)
        instance.face.addChild(innerBody)

        let innerBodyIndicator = new createjs.Shape()
        innerBodyIndicator.graphics
            .f(innerBodyIndicatorColor)
            .dc(0, 0, innerBodyIndicatorRadius)
        instance.face.addChild(innerBodyIndicator)
        instance.g.innerBodyIndicator = innerBodyIndicator

        let outerBody = new createjs.Shape()
        outerBody.graphics
            .ss(outerBodyWidth)
            .s(outerBodyColor)
            .arc(0, 0, outerBodyDistance, faceAngle, -faceAngle)
        instance.face.addChild(outerBody)

        let border = new createjs.Shape()
        border.graphics
            .ss(borderWidth)
            .s(borderColor)
            .arc(0, 0, outerBodyDistance, faceAngle, -faceAngle)
        instance.face.addChild(border)

        let hpBorder = new createjs.Shape()
        hpBorder.graphics
            .ss(borderWidth)
            .s(c.borderHpColor)
            .arc(0, 0, outerBodyDistance, faceAngle, -faceAngle)
        instance.face.addChild(hpBorder)
        instance.g.hpBorder = hpBorder

        let faceTop = new createjs.Shape()
        faceTop.graphics
            .ss(borderWidth)
            .s(faceColor)
            .arc(0, 0, outerBodyDistance, -faceAngle, 0)
        instance.face.addChild(faceTop)
        instance.g.faceTop = faceTop

        let faceBottom = new createjs.Shape()
        faceBottom.graphics
            .ss(borderWidth)
            .s(faceColor)
            .arc(0, 0, outerBodyDistance, faceAngle, 0, true)
        instance.face.addChild(faceBottom)
        instance.g.faceBottom = faceBottom

        instance.face.cache(-c.cacheRadius, -c.cacheRadius, c.cacheRadius*2, c.cacheRadius*2)

        /*
         * ADD CONTROL
         * */
        app.game.moveTo(instance.gfx, app.canvas_width/4, app.canvas_height/4)
        app.game.sortGfx(instance.gfx)

        addControl(instance)

        instance.maxHp = instance.config.hp
        instance.hp = instance.config.hp
        instance.takeDamage = takeDamage(instance)
        instance.applyForce = applyForce(instance)

        return instance
    }

    player.destroy = (instance) => {
        instance.destroyed = true

        TweenMax.to(instance.gfx, .25, {alpha: 0, delay: .25})
        TweenMax.to(instance.gfx, .5, {scaleX: 0, scaleY: 0, ease: Back.easeIn, onComplete:()=>{
            instance.config.radius = 0
            instance.gfx.x = -99999
            instance.gfx.y = -99999
        }})
    }

    const takeDamage = (instance) => (damage, hitPosition) => {
        instance.hp -= damage
        app.fx.getHit(instance, hitPosition)

        if(instance.hp <= 0)
            player.destroy(instance)
    }

    const applyForce = (instance) => (vector) => {
        instance.movement.force.x += vector.x
        instance.movement.force.y += vector.y
    }

    const leapSpecial = (() => {
        let leapTime = 0
        return (instance, e) => {
            ;({radius, leapSpecialLength, leapSpecialTime, leapSpecialCooldown} = instance.config)

            if(app.player.stopControl)
                return

            if(app.game.time - leapTime < leapSpecialCooldown*1000)
                return

            leapTime = app.game.time

            // LEAP
            let
                dir = app.game.normalize(instance.movement.vector)
            if(dir.x === 0 && dir.y === 0)
                return
            dir.x *= leapSpecialLength
            dir.y *= leapSpecialLength

            const
                newPos = {x: instance.gfx.x + dir.x, y: instance.gfx.y + dir.y}

            TweenMax.to(instance.gfx, leapSpecialTime, {x: newPos.x, y: newPos.y, ease: Power2.easeOut, onComplete: () => {
                app.player.stopControl = false
            }})

            app.player.stopControl = true
            app.game.gui.playerSpecial(leapTime)
        }
    })()

    const hitSpecial = (() => {
        let hitTime = 0
        return (instance, e) => {
            let c = instance.config
                ;({radius, hitReach, hitAngle, hitSpecialDamage, hitSpecialTime, hitSpecialCooldown} = instance.config)

            if(app.player.stopControl)
                return

            if(app.game.time - hitTime < hitSpecialCooldown*1000)
                return

            hitTime = app.game.time
            app.player.stopControl = true
            app.game.gui.playerSpecial(hitTime)

            const
                rotation = toRad(instance.face.rotation),
                hitCount = 5

            let hitInc = 0
            const hit = () => {
                app.game.gui.playerHit(app.game.time)
                app.action.playerHitArc(instance, c.specialHitWidth, hitAngle, c.hitSpecialDamage, {color: c.specialHitColor, radiusAdjust: c.specialHitRadiusAdjust})

                if(hitInc++ <= hitCount)
                    setTimeout(hit, hitSpecialTime/hitCount*1000)
                else
                    app.player.stopControl = false
            }
            hit()
        }
    })()

    const hitBasic = (() => {
        let hitTime = 0
        return (instance, e) => {
            let c = instance.config
                ;({radius, hitReach, hitAngle, hitDamage, hitCooldown} = instance.config)

            if(app.player.stopControl)
                return

            const hit = () => {
                hitTime = app.game.time
                app.game.gui.playerHit(hitTime)
                app.action.playerHitArc(instance, c.hitWidth, hitAngle, c.hitDamage, {radiusAdjust: c.hitRadiusAdjust})
            }

            let soften = hitCooldown < .5 ? .05 : .1
            if(app.game.time - hitTime < (hitCooldown-soften)*1000)
            {}
            else if(app.game.time - hitTime < hitCooldown*1000)
                setTimeout(hit, soften*1000)
            else
                hit()
        }
    })()

    let addControl = (instance) => {
        const moveFn = (bool) => (e) => {
            if(instance.destroyed)
                return

            let keyCode = e.keyCode
            if(keyCode === 37) // LEFT
                instance.moveDirC = bool
            else if(keyCode === 39) // RIGHT
                instance.moveDirCC = bool
            else if(keyCode === 40) // DOWN
                instance.moveDirBack = bool
            else if(keyCode === 38) // UP
                instance.moveDirForward = bool
        }
        $(window).on("keydown", moveFn(true))
        $(window).on("keyup", moveFn(false))

        $(window).on("mousedown", (e) => {
            if(instance.destroyed)
                return

            if(e.which === 1)
                hitBasic(instance, e)
            else if(e.which === 3) {
                hitSpecial(instance, e)
                // leapSpecial(instance, e)
            }
        })
    }

    const updatePosition = (instance, boss, deltaTime) => {
        ;({maxSpeed, drag, acceleration, radius} = instance.config)

        const
            moveDirForward = instance.moveDirForward,
            moveDirBack = instance.moveDirBack,
            moveDirC = instance.moveDirC,
            moveDirCC = instance.moveDirCC

        const bakeHandleMovementDistance = (maxDistance, drag, delta) => (distance, maxDistanceCheck = true) => {
            const dirPositive = distance > 0
            distance = maxDistanceCheck ?
                dirPositive ? Math.min(maxDistance, distance) : Math.max(-maxDistance, distance) :
                distance
            distance = dirPositive ? Math.max(0, distance - drag) : Math.min(0, distance + drag)
            distance = distance < delta && distance > -delta ? 0 : distance
            return distance
        }

        const
            bossAngle = app.game.getAngle(instance.gfx, boss.gfx),
            radialAngleStart = rad(bossAngle - Math.PI),
            bossDistance = app.game.distance(instance.gfx.x, instance.gfx.y, boss.gfx.x, boss.gfx.y),
            delta = .001

        const handleMovementDistance = bakeHandleMovementDistance(maxSpeed, drag, delta)

        // RADIAL MOVEMENT
        ;({radialVector, radialDistance} = (()=>{
            let
                accC = moveDirC ? acceleration : 0,
                accCC = moveDirCC ? -acceleration : 0,
                radialDistanceRaw = instance.movement.radial + accC + accCC,
                radialDistance = handleMovementDistance(radialDistanceRaw),

                movementAngle = radialDistance / bossDistance, // 2PIr=length => angle*r=length => angle=length/r
                angle = radialAngleStart + movementAngle,

                relatliveRadialPosition = {x: bossDistance * Math.cos(angle), y: bossDistance * Math.sin(angle)},
                newRadialPosition = {x: relatliveRadialPosition.x + boss.gfx.x, y: relatliveRadialPosition.y + boss.gfx.y},
                radialVector = instance.gfx.globalToLocal(newRadialPosition.x, newRadialPosition.y)

            return {radialVector, radialDistance}
        })())

        // CENTRIPETAL MOVEMENT
        ;({centripetalVector, centripetalDistance} = (()=>{
            let
                accF = moveDirForward ? acceleration : 0,
                accB = moveDirBack ? -acceleration : 0,

                centripetalDistanceRaw = accF + accB + instance.movement.centripetal,
                centripetalDistance = handleMovementDistance(centripetalDistanceRaw),

                centripetalVector = {x: centripetalDistance * Math.cos(bossAngle), y: centripetalDistance * Math.sin(bossAngle)}

            return {centripetalVector, centripetalDistance}
        })())

        // EXTERNAL FORCE
        const forceVector = (() => {
            const
                forceNormalized = app.game.normalize(instance.movement.force),
                forceDistanceRaw = app.game.vectorLength(instance.movement.force),
                forceDistance = handleMovementDistance(forceDistanceRaw, false),
                forceVector = {x: forceNormalized.x * forceDistance, y: forceNormalized.y * forceDistance}

            return forceVector
        })()

        // MOVE
        const moveVector = {x: centripetalVector.x + radialVector.x + forceVector.x, y: centripetalVector.y + radialVector.y + forceVector.y}

        instance.movement = {
            centripetal: centripetalDistance,
            radial: radialDistance,
            force: forceVector,
            vector: moveVector
        }

        if(!app.player.stopControl)
            app.game.translate(instance.gfx, moveVector.x, moveVector.y)

        // COLLISIONS
        let collisionObjects = app.game
            .collisionCircles(instance.gfx.x, instance.gfx.y, radius, instance.ID)
            .map(c=>c.gameObject)
            .filter(o=>o.hasBody)

        if(collisionObjects.length) {
            const x = instance.gfx.x, y = instance.gfx.y
            collisionObjects.forEach(gameObject => {
                const
                    worldPos = app.game.getWorldPosition(gameObject.gfx),
                    vector = app.game.getVector(worldPos.x, worldPos.y, x, y),
                    distance = app.game.distance(worldPos.x, worldPos.y, x, y),
                    pushDistance = Math.abs(distance - instance.config.radius - gameObject.config.radius),
                    vectorMultiplier = pushDistance/distance,
                    pushVector = {
                        x: vector.x * vectorMultiplier,
                        y: vector.y * vectorMultiplier
                    }
                app.game.translate(instance.gfx, pushVector.x, pushVector.y)
            })
        }
    }

    const updateRotation = (instance, boss, deltaTime) => {
        instance.face.rotation = app.game.getAngle(instance.gfx, boss.gfx, true)
    }

    player.update = (instance, boss, deltaTime) => {
        if(instance.destroyed)
            return

        updatePosition(instance, boss, deltaTime)
        updateRotation(instance, boss, deltaTime)

        // app.fx.movementTrail(instance, {})

        instance.face.updateCache()
    }

    // app.addInitCallback(player.init)

    /*
     * END
     * */
    return player
})(window.app.player, window.app, jQuery, createjs, TweenMax, EasingFunctions)