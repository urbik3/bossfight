"use strict"

window.app = window.app || {}
window.app.boss = window.app.boss || {}
window.app.boss.behaviour = window.app.boss.behaviour || {}

/*
 * BOSS BEHAVIOUR
 * */
window.app.boss.behaviour = ((behaviour, boss, app, $, createjs, TweenMax, EasingFunctions) => {

    let
        running = false,
        patterns = ["hitAndFollow", "closeDistanceAndStartHitting"],
        actualPattern = 0

    behaviour.update = (instance, player, dt) => {
        return instance.behaviour.update(instance, player, dt)

        // const changePattern = () => {
        //     if(!running)
        //         return
        //
        //     let newPattern = randomInt(0, patterns.length-1)
        //     while(newPattern === actualPattern)
        //         newPattern = randomInt(0, patterns.length-1)
        //
        //     actualPattern = newPattern
        //     setTimeout(changePattern, randomInt(10, 30) * 1000)
        //     console.log(patterns[actualPattern])
        // }
        //
        // if(!running) {
        //     running = true
        //     changePattern()
        // }
        //
        // return behaviour.pattern[patterns[actualPattern]](instance, player, dt)
    }

    /*
    * PATTERNS
    * */

    behaviour.pattern = behaviour.pattern || {}

    const makeDeltaT = (deltaTime) => (time) => {
        return deltaTime > time*1000
    }

    // CLOSE DISTANCE AND START HITTING
    behaviour.pattern.closeDistanceAndStartHitting = (() => {
        // INIT
        let deltaTime = 0,
            changeCount = 0

        return (instance, player, dt) => {
            deltaTime += dt

            let activity = [],
                changeConst = 0

            const
                deltaT = makeDeltaT(deltaTime),
                resetT = () => deltaTime = 0

            let minPlayerDistance = 30
            if(instance.behaviour.tags.has("PLAYER_CLOSE"))
                minPlayerDistance = 50

            // LOGIC
            if(app.game.distance(instance.gfx.x, instance.gfx.y, player.gfx.x, player.gfx.y) >= player.config.radius+instance.config.radius+minPlayerDistance) {
                activity = [a.facePlayer({}), a.followPlayer({})]
                instance.behaviour.tags.remove("PLAYER_CLOSE")
                resetT()
            }
            else {
                instance.behaviour.tags.addPerm("PLAYER_CLOSE")
                activity = [a.facePlayer({})]
                if(deltaT(2)) {
                    activity.push(a.hitArc(50, .25*Math.PI, 10, {}))
                    resetT()
                }
            }

            return activity
        }
    })()
    
    // HIT AND FOLLOW
    behaviour.pattern.hitAndFollow = (() => {
        // INIT
        let deltaTime = 0,
            changeCount = 0

        return (instance, player, dt) => {
            deltaTime += dt

            let activity = [],
                changeConst = 5

            const
                deltaT = makeDeltaT(deltaTime),
                change = () => {
                    changeCount++
                    deltaTime = 0
                }

            // LOGIC
            if(changeCount % changeConst === 0) {
                activity = [a.facePlayer({})]
                if(deltaT(3))
                    change()
            }
            else if(changeCount % changeConst === 1) {
                activity = []
                if(deltaT(1))
                    change()
            }
            else if(changeCount % changeConst === 2) {
                activity = [a.hitArc(50, .25*Math.PI, 10, {})]
                change()
            }
            else if(changeCount % changeConst === 3) {
                activity = []
                if(deltaT(.5))
                    change()
            }
            else if(changeCount % changeConst === 4) {
                activity = [a.facePlayer({}), a.followPlayer({})]
                if(deltaT(5))
                    change()
            }

            return activity
        }
    })()

    /*
    * ACTIVITIES
    * */

    behaviour.activity = behaviour.activity || {}
    let a = behaviour.activity // ALIAS

    behaviour.activity.facePlayer =
        ({rotationSpeed = 10, gfx = false}) =>
            (instance, player) => {
                let
                    bossPosition = instance.gfx,
                    playerPosition = player.gfx,
                    rotation = app.game.getAngle(bossPosition, playerPosition)

                if(gfx)
                    boss.rotate(instance, rotation, false, gfx)
                else
                    boss.rotate(instance, rotation)
                instance.behaviour.tags.add("FACE_PLAYER")
            }

    behaviour.activity.followPlayer =
        ({speed = false}) =>
            (instance, player) => {
                ;({speed} = instance.config)

                let
                    bossPosition = instance.gfx,
                    playerPosition = player.gfx,
                    dirVector = app.game.getVector(bossPosition.x, bossPosition.y, playerPosition.x, playerPosition.y),
                    distance = app.game.distance(bossPosition.x, bossPosition.y, playerPosition.x, playerPosition.y),
                    speedVector = {
                        x: dirVector.x / distance * speed,
                        y: dirVector.y / distance * speed
                    }

                app.game.translate(instance.gfx, speedVector.x, speedVector.y)
                instance.behaviour.tags.add("FOLLOW_PLAYER")
            }

    behaviour.activity.hitArc = app.action.bossHitArc
    //     (hitReach, hitAngle, hitDamage,
    //         {radiusAdjust = 0, rotationAdjust = 0, hitFx = true}) =>
    //     (instance) => {
    //         const
    //             rotation = instance.face.rotation + toDegree(rotationAdjust),
    //             radius = instance.config.radius + radiusAdjust
    //
    //         let hitArea
    //         if(hitFx)
    //             hitArea = (hitFx === true) ?
    //                 app.fx.hitArc(hitReach, hitAngle, {})(instance) :
    //                 hitFx(instance)
    //
    //         let collisions = app.game.collisionArcToCircles(instance.gfx.x, instance.gfx.y, radius+hitReach, toRad(rotation), hitAngle, instance.ID)
    //         collisions
    //             .filter(c => c.gameObject.hp)
    //             .forEach(c => {
    //                 c.gameObject.takeDamage(hitDamage, c.intersections.IS1)
    //                 instance.behaviour.tags.add("HIT_PLAYER")
    //             })
    //
    //         instance.behaviour.tags.add("HIT_BASIC", "HIT")
    //
    //         return {collisions, hitArea}
    // }

    behaviour.activity.hitCircle = app.action.bossHitCircle
        // (hitReach, hitDamage, {hitFx = true}) =>
        //     (instance) => {
        //         let hitArea
        //         if(hitFx)
        //             hitArea = (hitFx === true) ?
        //                 app.fx.hitCircle(hitReach, {})(instance) :
        //                 hitFx(instance)
        //
        //         let collisions = app.game.collisionCircles(instance.gfx.x, instance.gfx.y, instance.config.radius+hitReach, instance.ID)
        //         collisions
        //             .filter(c => c.gameObject.hp)
        //             .forEach(c => {
        //                 c.gameObject.takeDamage(hitDamage)
        //                 instance.behaviour.tags.add("HIT_PLAYER")
        //             })
        //
        //         instance.behaviour.tags.add("HIT_CIRCULAR", "HIT")
        //
        //         return {collisions, hitArea}
        //     }

    /*
    * TAGS
    * */

    behaviour.tags = behaviour.tags || {}
    ;(() => {
        let
            tags = [],
            tagsPerm = []

        behaviour.tags.add = (...addTags) =>
            tags.push(...addTags.filter(addTag => !tags.find(tag => tag === addTag)))

        behaviour.tags.addPerm = (...addTags) =>
            tagsPerm.push(...addTags.filter(addTag => !tagsPerm.find(tag => tag === addTag)))

        behaviour.tags.remove = (...removeTags) => {
            tags = tags.filter(rmTag(removeTags))
            tagsPerm = tagsPerm.filter(rmTag(removeTags))
        }
        const rmTag = removeTags => tag => !removeTags.find(removeTag => tag === removeTag)

        behaviour.tags.has = (...hasTags) =>
            !!tags.filter(hasTag(hasTags)).length ||
                !!tagsPerm.filter(hasTag(hasTags)).length
        const hasTag = (hasTags) => tag => hasTags.find(hasTag => tag === hasTag)

        behaviour.tags.clear = () =>
            tags = []

        behaviour.tags.get = () => {
            let merge = []
            merge.push(...tags, ...tagsPerm)
            return [...new Set(merge)]
        }
    })()

    /*
     * ACTION CONTROL
     * */

    behaviour.activity.startAction = (instance) => {
        if(instance.behaviour.tags.has("ACTION_ACTIVE"))
            return false
        instance.behaviour.tags.addPerm("ACTION_ACTIVE")
        return true
    }

    behaviour.activity.stopAction = (instance) => {
        instance.behaviour.tags.remove("ACTION_ACTIVE")
        instance.behaviour.tags.add("ACTION_END")
    }

    behaviour.activity.activeAction = (instance) =>
        instance.behaviour.tags.has("ACTION_ACTIVE")

    behaviour.activity.endAction = (instance) =>
        instance.behaviour.tags.has("ACTION_END")

    /*
     * END
     * */
    return behaviour
})(window.app.boss.behaviour, window.app.boss, window.app, jQuery, createjs, TweenMax, EasingFunctions)