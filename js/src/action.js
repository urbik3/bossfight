"use strict"

window.app = window.app || {}
window.app.action = window.app.action || {}

/*
 * EFFECTS
 * */
window.app.action = ((action, app, $, createjs, TweenMax, EasingFunctions) => {

    const hitArc = (instance, reach, angle, damage, options) => {
        let onHit = options.onHit

        app.fx.hitArc(instance, reach, angle, options)
            .then((result) => {
                if(result.type === "hit" && result.hits) {
                    if(onHit) onHit(result.hits)
                    result.hits.forEach(collision =>
                        collision.gameObject.takeDamage(damage,
                            app.game.getHitPosition(collision.intersections, collision.gameObject)))
                }
            })
    }

    action.playerHitArc = (instance, reach, angle, damage, options) => {
        let animationTime = .5,
            maxAnimationTimeSpeedUp = .35,
            distance = instance.movement.centripetal,
            maxDistance = instance.config.maxSpeed

        options.animationTime = animationTime - Math.max(0, maxAnimationTimeSpeedUp * (distance/maxDistance))

        hitArc(instance, reach, angle, damage, options)
    }

    action.bossHitArc = (reach, angle, damage, options) => instance => {
        instance.behaviour.tags.add("HIT_BASIC", "HIT")

        const addTag = ()=>instance.behaviour.tags.add("HIT_PLAYER")
        if(options.onHit) {
            const onHit = options.onHit
            options.onHit = (hits) => {
                addTag()
                onHit(hits)
            }
        }
        else
            options.onHit = addTag

        hitArc(instance, reach, angle, damage, options)
    }

    action.bossHitCircle = (reach, damage, options) => instance => {
        instance.behaviour.tags.add("HIT_CIRCULAR", "HIT")

        const addTag = ()=>instance.behaviour.tags.add("HIT_PLAYER")
        if(options.onHit) {
            const onHit = options.onHit
            options.onHit = (hits) => {
                addTag()
                onHit(hits)
            }
        }
        else
            options.onHit = addTag

        hitArc(instance, reach, "circle", damage, options)
    }

    /*
     * END
     * */
    return action
})(window.app.action, window.app, jQuery, createjs, TweenMax, EasingFunctions)