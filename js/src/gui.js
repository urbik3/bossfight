"use strict"

window.app = window.app || {}
window.app.game = window.app.game || {}
window.app.game.gui = window.app.game.gui || {}

/*
 * DOUBLE RANDOM
 * */
window.app.game.gui = ((gui, game, app, $, createjs, EasingFunctions) => {

    gui.config = (()=>{
        let obj = {
            padding: 20,
            width: 200,
            height: 20,
            strokeWidth: 2
        }
        obj.widthHalf = obj.width/2-obj.padding/2
        return obj
    })()

    gui.init = () => {
        let gfx = new createjs.Container()
        gui.gfx = gfx
        gfx.x = gui.config.padding
        gfx.y = app.canvas_height-gui.config.padding
        app.stage.addChild(gfx)

        hitCD()
        specialCD()
        hpPlayer()
        hpBoss()
    }

    gui.resize = () => {
        gui.gfx.x = gui.config.padding
        gui.gfx.y = app.canvas_height-gui.config.padding
    }

    gui.update = (player, boss, deltaTime) => {
        hitTime = updateHitCooldown(hitTime, player.config.hitCooldown, player)
        specialTime = updateSpecialCooldown(specialTime, player.config.specialCooldown, player)
        updateHpPlayer(player.maxHp, player.hp, player)
        updateHpBoss(boss.maxHp, boss.hp, boss)
    }

    /*
     * HIT CD
     * */
    const hitCD = () => {
        const
            boxY = getBoxY(3)

        let hitCDBox = new createjs.Shape()
        hitCDBox.graphics
            .s("#0000ff")
            .ss(gui.config.strokeWidth)
            .dr(0, 0, gui.config.widthHalf, gui.config.height)
        hitCDBox.x = 0
        hitCDBox.y = boxY
        gui.gfx.addChild(hitCDBox)

        let hitCD = new createjs.Shape()
        hitCD.graphics
            .f("#0000ff")
            .dr(0, 0, gui.config.widthHalf, gui.config.height)
        hitCD.x = 0
        hitCD.y = boxY
        gui.gfx.addChild(hitCD)
        gui.gfx.hitCD = hitCD
    }

    let hitTime = false
    gui.playerHit = (ht) => {
        hitTime = ht
    }

    const updateHitCooldown = (hitTime, cd, player) => {
        if(!hitTime)
            return false

        const
            CD = cd*1000

        if(game.time - hitTime >= CD) {
            gui.gfx.hitCD.alpha = 1
            gui.gfx.hitCD.graphics.command.w = gui.config.widthHalf
            player.g.faceTop.graphics.command.endAngle = 0
            player.g.faceBottom.graphics.command.endAngle = 0
            hitTime = false
            return hitTime
        }

        gui.gfx.hitCD.alpha = .5
        gui.gfx.hitCD.graphics.command.w = EasingFunctions.linear((game.time-hitTime) / CD) * gui.config.widthHalf

        player.g.faceTop.graphics.command.endAngle = (EasingFunctions.easeInQuad((game.time-hitTime) / CD) * (player.config.faceAngle)) - player.config.faceAngle
        player.g.faceBottom.graphics.command.endAngle = (EasingFunctions.easeInQuad((game.time-hitTime) / CD) * (-player.config.faceAngle)) + player.config.faceAngle

        return hitTime
    }

    /*
     * SPECIAL CD
     * */
    const specialCD = () => {
        const
            boxY = getBoxY(3)

        let specialCDBox = new createjs.Shape()
        specialCDBox.graphics
            .s("#0000ff")
            .ss(gui.config.strokeWidth)
            .dr(0, 0, gui.config.widthHalf, gui.config.height)
        specialCDBox.x = gui.config.widthHalf+gui.config.padding
        specialCDBox.y = boxY
        gui.gfx.addChild(specialCDBox)

        let specialCD = new createjs.Shape()
        specialCD.graphics
            .f("#0000ff")
            .dr(0, 0, gui.config.widthHalf, gui.config.height)
        specialCD.x = gui.config.widthHalf+gui.config.padding
        specialCD.y = boxY
        gui.gfx.addChild(specialCD)
        gui.gfx.specialCD = specialCD
    }

    let specialTime = false
    gui.playerSpecial = (st) => {
        specialTime = st
    }

    const updateSpecialCooldown = (specialTime, cd, player) => {
        if(!specialTime)
            return false

        const
            CD = cd*1000

        if(game.time - specialTime >= CD) {
            gui.gfx.specialCD.alpha = 1
            gui.gfx.specialCD.graphics.command.w = gui.config.widthHalf
            player.g.innerBodyIndicator.visible = true
            specialTime = false
            return specialTime
        }

        gui.gfx.specialCD.alpha = .5
        gui.gfx.specialCD.graphics.command.w = EasingFunctions.linear((game.time-specialTime) / CD) * gui.config.widthHalf
        player.g.innerBodyIndicator.visible = false

        return specialTime
    }

    /*
     * PLAYER HP
     * */
    const hpPlayer = () => {
        const
            boxY = getBoxY(2)

        let hpPlayerBox = new createjs.Shape()
        hpPlayerBox.graphics
            .s("#00ff00")
            .ss(gui.config.strokeWidth)
            .dr(0, 0, gui.config.width, gui.config.height)
        hpPlayerBox.x = 0
        hpPlayerBox.y = boxY
        gui.gfx.addChild(hpPlayerBox)

        let hpPlayer = new createjs.Shape()
        hpPlayer.graphics
            .f("#00ff00")
            .dr(0, 0, gui.config.width, gui.config.height)
        hpPlayer.x = 0
        hpPlayer.y = boxY
        gui.gfx.addChild(hpPlayer)
        gui.gfx.hpPlayer = hpPlayer

        gui.hpPlayer = {
            toWidth: gui.config.width,
            animationTime: .5,
            animationStart: 0
        }
        gui.changePlayerHPBorderAngle = true
    }

    const updateHpPlayer = (maxHp, hp, player) => {
        const
            currentWidth = Math.max(0, EasingFunctions.linear(hp/maxHp) * gui.config.width)

        if(gui.hpPlayer.toWidth !== currentWidth) {
            gui.hpPlayer.animationStart = game.time
            gui.hpPlayer.fromWidth = gui.gfx.hpPlayer.graphics.command.w
            gui.hpPlayer.toWidth = currentWidth
        }
        else if(gui.hpPlayer.toWidth !== gui.gfx.hpPlayer.graphics.command.w) {
            gui.gfx.hpPlayer.graphics.command.w =
                gui.hpPlayer.fromWidth -
                (gui.hpPlayer.fromWidth-gui.hpPlayer.toWidth) *
                EasingFunctions.easeOutCubic(Math.min(1, (game.time-gui.hpPlayer.animationStart) / (gui.hpPlayer.animationTime*1000)))
        }
        else {
            gui.hpPlayer.toWidth = currentWidth
            gui.gfx.hpPlayer.width = currentWidth
        }

        // ON PLAYER GUI
        const
            getHPBorderAngle = () => ( ( (player.maxHp - player.hp) / player.maxHp) * (Math.PI-.001 - player.config.faceAngle)) + player.config.faceAngle
        if(getHPBorderAngle() !== player.g.hpBorder.graphics.command.startAngle && gui.changePlayerHPBorderAngle) {
            gui.changePlayerHPBorderAngle = false
            setTimeout(() => {
                player.g.hpBorder.graphics.command.startAngle = getHPBorderAngle()
                player.g.hpBorder.graphics.command.endAngle = -getHPBorderAngle()
                gui.changePlayerHPBorderAngle = true
            }, 300)
        }
    }

    /*
     * BOSS HP
     * */
    const hpBoss = () => {
        const
            boxY = getBoxY(1)

        let hpBossBox = new createjs.Shape()
        hpBossBox.graphics
            .s("#ff0000")
            .ss(gui.config.strokeWidth)
            .dr(0, 0, gui.config.width, gui.config.height)
        hpBossBox.x = 0
        hpBossBox.y = boxY
        gui.gfx.addChild(hpBossBox)

        let hpBoss = new createjs.Shape()
        hpBoss.graphics
            .f("#ff0000")
            .dr(0, 0, gui.config.width, gui.config.height)
        hpBoss.x = 0
        hpBoss.y = boxY
        gui.gfx.addChild(hpBoss)
        gui.gfx.hpBoss = hpBoss
        
        gui.changeBossHPBorderAngle = true
    }

    const updateHpBoss = (maxHp, hp, boss) => {
        gui.gfx.hpBoss.graphics.command.w = Math.max(0, EasingFunctions.linear(hp / maxHp) * gui.config.width)

        // ON PLAYER GUI
        const
            getHPBorderAngle = () => ( ( (boss.maxHp - boss.hp) / boss.maxHp) * (Math.PI-.001 - boss.config.faceAngle)) + boss.config.faceAngle
        if(getHPBorderAngle() !== boss.g.hpBorder.graphics.command.startAngle && gui.changeBossHPBorderAngle) {
            gui.changeBossHPBorderAngle = false
            setTimeout(() => {
                boss.g.hpBorder.graphics.command.startAngle = getHPBorderAngle()
                boss.g.hpBorder.graphics.command.endAngle = -getHPBorderAngle()
                gui.changeBossHPBorderAngle = true
            }, 300)
        }
    }

    /*
    * TOOLS
    * */

    const getBoxY = num => -gui.config.height*num - (gui.config.padding*(num-1))

    /*
    * END
    * */

    return gui

})(window.app.game.gui, window.app.game, window.app, jQuery, createjs, EasingFunctions)