"use strict"

window.app = window.app || {}
window.app.game = window.app.game || {}
window.app.game.gui = window.app.game.gui || {}
window.app.player = window.app.player || {}
window.app.boss = window.app.boss || {}

/*
 * GAME
 * */
window.app.game = ((game, app, $, createjs, EasingFunctions) => {

    let boss,
        player,
        gameObjects = []

    game.time = 0

    game.init = () => {
        game.time = Date.now()
        lastTime = game.time

        boss = app.boss.create()
        player = app.player.create()

        gameObjects.push(player, boss/*, boss.leftLimb, boss.rightLimb*/)

        game.gui.init(player, boss)
    }

    game.resize = () => {
        game.gui.resize()
    }

    let lastTime = game.time,
        lastUpdate = true
    game.update = (deltaTime) => {
        game.time += deltaTime

        if(player.destroyed || boss.destroyed) {
            if(lastUpdate) {
                game.gui.update(player, boss, deltaTime)
                lastUpdate = false
            }
            return
        }

        // if(game.time - lastTime > 2000) {
        //     lastTime = game.time
        //     const
        //         angle = randomInt(0, 200)/100*Math.PI,
        //         x = player.gfx.x + Math.cos(angle) * player.config.radius,
        //         y = player.gfx.y + Math.sin(angle) * player.config.radius
        //     player.takeDamage(1, {x, y})
        // }

        app.player.update(player, boss, deltaTime)
        app.boss.update(boss, player, deltaTime)

        game.gui.update(player, boss, deltaTime)
    }

    /*
    * HELP FUNCTIONS
    * */

    // GAME OBJECTS
    game.getGameObjects = () => gameObjects
    game.addGameObject = (...args) => gameObjects.push(...args)
    game.removeGameObject = ID => gameObjects.splice(gameObjects.findIndex(go=>go.ID===ID), 1)

    // POSITION
    game.moveTo = (graphics, x, y) => {
        graphics.x = x
        graphics.y = y
    }

    game.translate = (graphics, x, y) => {
        graphics.x += x
        graphics.y += y
    }

    game.getWorldPosition = obj =>
        obj.localToGlobal(0, 0)

    // COLLISIONS
    const getIntersectionPoints = (x, y, radius, gameObject) => {
        const
            worldPos = game.getWorldPosition(gameObject.gfx),
            centerDistance = game.distance(x, y, worldPos.x, worldPos.y),
            pointVDistance = (radius**2 - gameObject.config.radius**2 + centerDistance**2) / (centerDistance*2),
            vToIntersectionDistance = Math.sqrt(radius**2 - pointVDistance**2),
            pointV = {
                x: x + pointVDistance * (worldPos.x - x) / centerDistance,
                y: y + pointVDistance * (worldPos.y - y) / centerDistance
            },
            IS1 = {
                x: pointV.x + vToIntersectionDistance * (y - worldPos.y) / centerDistance,
                y: pointV.y - vToIntersectionDistance * (x - worldPos.x) / centerDistance
            },
            IS2 = {
                x: pointV.x - vToIntersectionDistance * (y - worldPos.y) / centerDistance,
                y: pointV.y + vToIntersectionDistance * (x - worldPos.x) / centerDistance
            }

        return {IS1, IS2}
    }

    game.collisionCircles = (x, y, radius, ID = null) =>
        gameObjects
            .filter(o=>o.ID!==ID)
            .filter(o=>o.gfx)
            .filter(o => {
                const worldPos = game.getWorldPosition(o.gfx)
                return game.distance(x, y, worldPos.x, worldPos.y) < radius+o.config.radius
            })
            .map(gameObject => ({
                gameObject,
                intersections: getIntersectionPoints(x, y, radius, gameObject)
            }))

    game.collisionArcToCircles = (x, y, radius, rotation, hitAngle, ID = null) =>
        game
            .collisionCircles(x, y, radius, ID)
            .map(collision => {
                ;({IS1, IS2} = collision.intersections)

                // intersection points inside arc
                const
                    angle1 = game.getAngle({x, y}, IS1),
                    angle2 = game.getAngle({x, y}, IS2),
                    maxAngle = rotation+hitAngle,
                    minAngle = rotation-hitAngle,
                    hitIS1 = radBetween(angle1, minAngle, maxAngle),
                    hitIS2 = radBetween(angle2, minAngle, maxAngle)

                // // arc inside circle
                // const
                //     angleSwitch = Math.abs(angle1 - angle2) > Math.PI,
                //     angleBottom = angle1 < angle2 ? (angleSwitch?angle2:angle1) : (angleSwitch?angle1:angle2),
                //     angleTop = angle1 > angle2 ? (angleSwitch?angle2:angle1) : (angleSwitch?angle1:angle2),
                //     arcInsideCircle = radBetween(rotation, angleBottom, angleTop)

                // return hitIS1 || hitIS2 || arcInsideCircle

                let intersections = {}
                if(hitIS1)
                    intersections.IS1 = IS1
                if(hitIS2)
                    intersections.IS2 = IS2

                collision.intersections = intersections

                return collision
            })
            .filter(c => c.intersections.IS1 || c.intersections.IS2)
    
    game.getHitPosition = (intersections, hitObject) => {
        if(!intersections.IS2 && !intersections.IS1)
            return false

        if(!intersections.IS1 && intersections.IS2)
            return intersections.IS2
        else if(!intersections.IS2 && intersections.IS1)
            return intersections.IS1

        const
            IS1angle = app.game.getAngle(hitObject.gfx, intersections.IS1),
            IS2angle = app.game.getAngle(hitObject.gfx, intersections.IS2),
            isIS1Higher = rad(IS1angle - IS2angle) < Math.PI,
            lowerAngle = isIS1Higher ? IS2angle : IS1angle,
            higherAngle = isIS1Higher ? IS1angle : IS2angle,
            diffAngle = rad(higherAngle - lowerAngle),
            middleAngle = lowerAngle + diffAngle/2,
            relativeHitPosition = {x: hitObject.config.radius * Math.cos(middleAngle), y: hitObject.config.radius * Math.sin(middleAngle)},
            hitPosition = {x: relativeHitPosition.x + hitObject.gfx.x, y: relativeHitPosition.y + hitObject.gfx.y}

        return hitPosition
    }

    // MATH
    game.distance = (x1, y1, x2, y2) =>
        Math.sqrt((x1-x2)**2+(y1-y2)**2)

    game.getVector = (x1, y1, x2, y2) =>
        ({x: (x2-x1), y: (y2-y1)})

    game.vectorLength = ({x, y}) =>
        game.distance(0, 0, x, y)

    game.normalize = ({x, y}) => {
        const length = Math.sqrt(x**2 + y**2)
        if(length <= 0)
            return {x: 0, y: 0}
        return {x: x / length, y: y / length}
    }

    game.polarToCartesian = (r, angle) => ({x: r * Math.cos(angle), y: r * Math.sin(angle)})

    // @sectorAngle: angle between origin, target and origin x+ (point on the right of origin)
    game.getAngle = (coordsOrigin, coordsTarget, degree = false) => {
        const
            sector = (coordsOrigin.x > coordsTarget.x ? "left" : "right") + (coordsOrigin.y > coordsTarget.y ? "top" : "bottom"),
            sideX = Math.abs(coordsOrigin.x - coordsTarget.x),
            sideY = Math.abs(coordsOrigin.y - coordsTarget.y),
            sectorAngle = Math.atan(sideY/sideX)

        let angle =
            sector === "righttop" ? -sectorAngle + Math.PI*2 :
                sector === "rightbottom" ? sectorAngle :
                    sector === "leftbottom" ? -sectorAngle + Math.PI :
                        sector === "lefttop" ? sectorAngle + Math.PI :
                            NaN
        if(degree)
            angle = toDegree(angle)

        return angle
    }

    game.getVectorAngle = (vectorCoords, degree = false) =>
        game.getAngle({x: 0, y: 0}, vectorCoords, degree)

    // OTHER
    const depthCompare = (a, b) => {
        if(a.z < b.z)
            return -1
        if(a.z > b.z)
            return 1
        return 0
    }
    game.sortGfx = (gfx) =>
        gfx.sortChildren(depthCompare)

    app.addInitCallback(game.init)

    /*
    * END
    * */
    return game
})(window.app.game, window.app, jQuery, createjs, EasingFunctions)