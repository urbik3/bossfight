<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS -->
    <link href="css/app.css?cb=1510903848" rel="stylesheet">
    <!-- JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js"></script>
    <script src="js/app.js?cb=1510904151"></script>
    <!-- SOCIAL -->
    <title>BossFight</title>
</head>
<body>

<script>
    app.init()
</script>

</body>
</html>